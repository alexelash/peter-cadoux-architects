<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Peter Cadoux Architects
 */

?>

	</div><!-- #content -->
</div><!-- #page -->
	<script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.15.0/TweenMax.min.js"></script>
	<script type="text/javascript">
		var siteURL = "<?php echo home_url( $path, $scheme ); ?>/";
	</script>
		
<?php wp_footer(); ?>

</body>
</html>
