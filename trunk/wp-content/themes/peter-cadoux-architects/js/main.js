var isIntro,
		offset = $('.site-navigation').height(),
		newUrlTitle = window.location.href.substr(window.location.href.lastIndexOf('/') + 1),
		currentSlide = 0,
		History = window.History,
    State = History.getState(),
    isSmScreen = $(window).width()<680,
    isTouch = $('html').hasClass('touch'),
    isSkip,
    originalTitleTag = 'Peter Cadoux Architects',
    iOS = /iPad|iPhone|iPod/.test(navigator.platform);

$(document).ready(function(){
	isSkip = false;
	if (isSmScreen) isSkip = true;
	if (!$('html').hasClass('touch')) initScrollMagic();
	initCycle($('#homepage-slideshow'));
	initIntro(isSkip);
	scrollFunctions();
	activateNav();
	initMobileNav();
	// anchorScroll();
	$('a.project.thumb').click(onProjectLinkClick);

	if (isSmScreen && iOS) {
		$('#homepage-slideshow').css('height', '68vh');
	}
});
$(window).on('load', function(){
	initHistory();
	imagesLoaded($('img.loader-image'));
	jumpToInitial();
});

$(window).resize(function(){
  isSmScreen = $(window).width()<680;
})

function initMobileNav(){
	$('#hamburger-button').on('click', function() {
		if ($('.navigation-list').hasClass('open')) $('.navigation-list').removeClass('open');
		else $('.navigation-list').addClass('open');
	})
}

function shareButton(){
	if ($('html').hasClass('touch') && isSmScreen) {
		$('.image-holder').bind('tap', function(){
			$('.social-links').removeClass('sharelinksShow');
			// if ($(this).find('.social-links').hasClass('sharelinksShow')) $(this).find('.social-links').removeClass('sharelinksShow');
			$(this).find('.social-links').addClass('sharelinksShow');
		});
	} else if ($('html').hasClass('touch') && !isSmScreen) {
		$('.share-button').bind('tap', function(){
			$('.cycle-slide-active').find('.social-links').addClass('openedSocial');
		});
	} else {
		$('.share-button').on('mouseenter', function() {
			$('.cycle-slide-active').find('.social-links').addClass('openedSocial');
		});
		$('.image-holder').on('mouseenter', function() {
			$('.cycle-slide-active').find('.social-links').removeClass('openedSocial');
		});
	}
}

function imagesLoaded(theImage){
	theImage.imagesLoaded().progress( function( instance, image ) {
		$(image.img).parent().addClass('isLoaded');
	});
}

function activateNav(){
	$('.site-navigation li').each(function(){
		var href = $(this).find('a').attr('href');
		var slug = href.substring(href.lastIndexOf('/')+1);
		var topOffset = $('body').hasClass('openedSlideshow')?'0':$('.site-navigation').outerHeight();

		$(this).addClass(slug);

		$(this).find('a').on('click',function(e){
			e.preventDefault();
			$('nav').find('li').removeClass('current_page_item');
			$(this).closest('li').addClass('current_page_item');
			if (slug==('projects')) {
				// $('html,body').animate({ scrollTop:$("#projects-con").offset().top-topOffset }, 1000);
				$('body').removeClass('openedSlideshow').find('#project-holder').removeClass('slideshowGrow')
				$.waypoints('refresh');
			}
			$('html,body').animate({ scrollTop:$("#"+slug).offset().top-topOffset }, 1000);
			if (isSmScreen && $('.navigation-list').hasClass('open')) $('.navigation-list').removeClass('open');
			return false;
		});
	});
	$('.nav-logo').on('click', function(e) {
		e.preventDefault();
		$('body').removeClass('openedSlideshow').find('#project-holder').removeClass('slideshowGrow')
		$.waypoints('refresh');
		$('html,body').animate({scrollTop:$("section#homepage").offset().top}, 1000);
		return false;
	});
}

function initHistory(){
		History.Adapter.bind(window,'statechange',function(e){
		var state = History.getState();
		var newUrlTitle = window.location.href.substring(siteURL.length);
		newUrlTitle = newUrlTitle.substring(newUrlTitle.lastIndexOf('/')+1);

		var jumpTo = $('section[data-post-name="'+newUrlTitle+'"]');

		if(jumpTo.length){
			var jumpToY = jumpTo.offset().top-$('.site-navigation').outerHeight();
		}
	});
}

function jumpToInitial(){
	var jumpTo;
	var newUrlTitle = window.location.href.substring(siteURL.length);

	if (newUrlTitle.indexOf('projects')>-1){
  // console.log(newUrlTitle.indexOf('projects')>-1);

		loadProjectByURL(siteURL+'index.php/'+newUrlTitle, true, newUrlTitle.lastIndexOf('/')+1);
		jumpTo = $('section#projects');
	}
	else{
		newUrlTitle = newUrlTitle.substring(newUrlTitle.lastIndexOf('/')+1).replace(/\//g,'');
		jumpTo = $('section[data-post-name="'+newUrlTitle+'"]');
	}
	if(jumpTo.length){
		var jumpToY = jumpTo.offset().top-$('.site-navigation').outerHeight();
		$("html,body").scrollTop(jumpToY);
		setTimeout('initWaypoints();',2000);
		// console.log(jumpTo);

		History.pushState({snappedTo:jumpTo.data('post-name')}, jumpTo.data('sitename'), newUrlTitle);
	}
	else{
		initWaypoints();
	}
}

function initWaypoints(){
	// console.log('waypoints clicked in');
	$('section, div#project-holder, div#projects-con').waypoint(function(direction) {
		$('.site-navigation').removeClass('transparent');
		if ($(this).attr('id')==('project-holder')) $('.site-navigation').addClass('transparent');
		if ($(this).is('div#project-holder')) return;

		if(direction=="down") {
			onSectionChange($(this));
			setCurNavLink($(this).attr('id'));
		}
	}, {
		offset: '10%'
	});
	$('section, div#project-holder, div#projects-con').waypoint(function(direction) {
		$('.site-navigation').removeClass('transparent');
		if ($(this).attr('id')==('project-holder')) $('.site-navigation').addClass('transparent');
		if ($(this).is('div#project-holder')) return;

		if (direction=="up") {
			onSectionChange($(this));
			setCurNavLink($(this).attr('id'));
		}
	}, {
	  offset: function() {
	    return -$(this).height() + $(window).height()*.1;
	  }
	});
}

function setCurNavLink(id){
	$('nav').find('li').removeClass('current_page_item');
	if (id=='projects-con') $('li.projects').addClass('current_page_item');
	else $('li.'+id).addClass('current_page_item');
	console.log(id);
}

function onProjectLinkClick(e){
	e.preventDefault();
	$('#projects').data('post-name','projects/'+$(this).data('post-name'));
	loadProjectByURL($(this).attr('href'), false, $(this).data('post-name'));
	$('.site-navigation').addClass('transparent');

	return false;
}

function loadProjectByURL(theURL, projectRefresh, projSlug){
	$.ajax({
		url:theURL,
		context:$('#project-holder'),
		beforeSend: function() {
			$('.project.thumb[data-post-name='+projSlug+']').append( '<div class="loader">Loading Project...</div>' );
		},
		success: function( result ) {
			console.log(theURL);
			$('.loader').remove();
			$('#project-holder').empty().append($(result).find('#main').html()).show();
			if(projectRefresh) {
				$('#projects').attr('data-post-name','projects/'+$(this).find('.project-slideshow').attr('rel'));
			}
			if ($('#project-holder').length) {
				$('html,body').animate({scrollTop:$('#project-holder').offset().top}, 500);
			}
			onSectionChange($('section#projects'));
			initProject();
			$.waypoints('refresh');
			if (!isSmScreen && !isTouch) initCycle($(this).find('.slideshow'));
			else if (!isSmScreen && isTouch) {
				initSwipe($(this).find('.slideshow'));
				$('#project-holder').append( '<div class="swipe-instructional">Swipe to view project</div>' );
				imagesLoaded($(this).find('.slideshow').find('.slide').each(function(){$(this).find('img.loader-image')}));
			}
			shareButton();
			projectRefresh = false;
		},
		error: function(result) {console.log(theURL);}
	});
}

// var originalTitleTag = document.title;
function onSectionChange(newSection){
	var newString = siteURL+newSection.data('post-name');
	// console.log(newSection.attr('id'));
	History.pushState({snappedTo:newSection.data('post-name')}, newSection.data('sitename'), newString);
}

function initCycle(slideshow){
	slideshow.each(function(){
		$(this).cycle({
			 slides: ".slide",
			 timeout: 3000,
			 manualSpeed: 800,
			 speed: 3000,
			 prev: '.left-arrow',
			 next: ".image-holder, .right-arrow",
			 paused: true,
			 log: false
		});
		$(document).keyup(function (e) {
			if (e.keyCode == 37) $('#project-holder').find(slideshow).cycle('prev');
			else if (e.keyCode == 39) $('#project-holder').find(slideshow).cycle('next');
		});
		imagesLoaded($(this).find('img.loader-image'));
		slideshow.on( 'cycle-before', function( event, opts , outgoingSlideEl, incomingSlideEl, forwardFlag) {
			var currentSlideIndex = slideshow.find('.cycle-slide:not(.cycle-sentinel)').index($(incomingSlideEl));
			for (var i = currentSlideIndex-2; i < currentSlideIndex+2; i++) {
				var futureSlideImg = slideshow.find('.cycle-slide:not(.cycle-sentinel)').eq(i).find('img.loader-image');
	      futureSlideImg.attr('src', futureSlideImg.data('src')).css('width','auto');
	      // futureSlideImg.imagesLoaded().progress( function( instance, image ) {
	      //     $(image.img).parent().addClass('isLoaded');

	      // });
				// console.log(futureSlideImg);
				imagesLoaded(futureSlideImg);
			}
		});
	});
	initNum($('#denom').closest('#project-holder').find(slideshow));
}
function initNum(slideshow){
	var slides = slideshow.find('.slide'),
			numImages = slides.length;

	slideshow.on('cycle-before', onSlideshowChange);
	$('#denom').html(numImages);

	function onSlideshowChange(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag){
		$('#numer').html(optionHash.nextSlide+1);
	}
}

function initSwipe(slideshow){
	var currentImg = 0;
	slideshow.delay(1000).fadeIn(1000);
	slideshow.addClass('swiping');
	slideshow.width($(window).width());
	var IMG_WIDTH = slideshow.width();
	var imgs = slideshow.find('.slide'),
			numImages=imgs.length,
			speed=500;
	$('#denom').html(numImages);
	imgs.width(IMG_WIDTH).css('float','left').height($(window).height());
	slideshow.width(numImages*IMG_WIDTH);

	var swipeOptions={
		triggerOnTouchEnd: true,
		swipeStatus: swipeStatus,
		threshold: 75
	};
	if(currentImg!=0) scrollImages( IMG_WIDTH * currentImg, 0);
	else updateImage(slideshow, 'right');

	imgs.swipe( swipeOptions );

	function swipeStatus(event, phase, direction, distance){
		if( phase=="move" && (direction=="left" || direction=="right") ){
			scrollImages((IMG_WIDTH * currentImg) + (direction == "left"?distance:-1*distance), 0);
			$('.swipe-instructional').fadeOut();

		}
		else if ( phase == "cancel") scrollImages(IMG_WIDTH * currentImg, speed);
		else if ( phase =="end" ) updateImage(slideshow,direction); 
	}

	function updateImage(slideshow, dir){
		currentImg = (dir == "right" ? Math.max(currentImg-1, 0) : Math.min(currentImg+1, numImages-1));
		for (var i = imgs.length - 1; i >= 0; i--) {
			if(2>Math.abs(i-currentImg)) $(imgs[i]).removeClass('showing');
			else $(imgs[i]).addClass('showing');
			$('#numer').html(currentImg+1);
		}
		if(currentImg>0 || numImages<2) $('.cycle-title span, .swipe-instructional').hide();
		slideshow.find('.slide').removeClass('showing');
		slideshow.find('.slide').eq(currentImg).addClass('showing');
		scrollImages( IMG_WIDTH * currentImg, speed);
		// $('#swipe-caption span.update').html($('.slideshow .slide').eq(currentImg).attr('data-cycle-title'))
	}

	function scrollImages(distance, duration){
		imgs.css("-webkit-transition-duration", (duration/1000).toFixed(1) + "s");
		var value = (distance<0 ? "" : "-") + Math.abs(distance).toString();
		imgs.css("-webkit-transform", "translate3d("+value +"px,0px,0px)");
	}
}

function scrollFunctions(){
	$(window).scroll(function(){
		if ($(this).scrollTop() > $(window).height()*.15) {
			$('.instruction').removeClass('show');
		}
		if($(window).scrollTop()>$(window).height()*.75) $('.site-navigation').addClass('showLogo');
		else $('.site-navigation').removeClass('showLogo');
	});
}

function initProject(){
	var container = $('#project-holder');
	var projSlideshow = container.find('.slideshow');
	var projHeight = $(window).height();

	$(window).resize(function(){
		var projHeight = $(window).height();
		if (!isSmScreen) projSlideshow.height(projHeight);
		// if ($('#project-holder').length) $("html, body").animate({ scrollTop: $('.project-slideshow').offset().top}, 0);
	});

	container.addClass('slideshowGrow').closest('body').addClass('openedSlideshow');
	$('.project-title').addClass('visible');
	if (!isSmScreen) projSlideshow.height(projHeight)

	// $('a.close-btn').on('click', function(e) {
	// 	e.preventDefault();
	// 	$(this).closest(container).removeClass('slideshowGrow openDesc').closest('body').addClass('openedSlideshow');
	// 	$('.project-title').removeClass('visible')
	// 	projSlideshow.height(0);
	// });
}

function initIntro(isSkip){
	if (isSkip == true) {
		$('body').addClass('introComplete');
		$('#homepage-slideshow').cycle('resume');
	}
	$('body').delay(2000).queue(function(next){
		$(this).addClass('introComplete');
		next();
	});
	isIntro = true;
	$('#homepage-slideshow').delay(5000).queue(function(next){
		$(this).cycle('resume');
		next();
	});
}

function onIntroComplete(){
	$('#intro').fadeOut(isSkip?0:2000);
	$('a.logo, .open, .directional').addClass('makeVisible');
	$('header').removeClass('during-intro');
	$('section').removeClass('hidden');
}

function initScrollMagic(){
	controller = new ScrollMagic();

	$('section').each(function() {
		var timeline, scene, tweens;
		timeline = new TimelineMax();
		parallaxFactor = $(window).height()*0.5;
		tweens = [];

		$(this).find('.section-title').each(function(i) {
			tweens.push(TweenMax.fromTo($(this), .5, {alpha: 0, y:25},  {delay:.35, alpha:1, y:0, force3D:true, ease: Linear.ease}));
		});

		timeline.add(tweens);
		scene = new ScrollScene({
			triggerElement: $(this),
			triggerHook: 1,
			reverse: true
		});
		scene.setTween(timeline).addTo(controller);
	});
}