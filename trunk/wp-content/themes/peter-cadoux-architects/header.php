<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Peter Cadoux Architects
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="icon" href="<?php echo site_url() ?>/favicon.png">
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo site_url() ?>/apple-touch-icon-57x57-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo site_url() ?>/apple-touch-icon-60x60-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo site_url() ?>/apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo site_url() ?>/apple-touch-icon-76x76-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo site_url() ?>/apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo site_url() ?>/apple-touch-icon-120x120-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo site_url() ?>/apple-touch-icon-144x144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="180x180" href="<?php echo site_url() ?>/apple-touch-icon-180x180-precomposed.png">

<!-- Minion Pro, TYPEKIT -->
<script src="https://use.typekit.net/gyr8hxe.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  // ga('create', 'UA-13203844-35', 'auto');
  ga('send', 'pageview');
</script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'peter-cadoux-architects' ); ?></a>

	<header class="navigation-wrapper">
		<nav class="site-navigation">
			<a id="mobile-logo" class="nav-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php get_template_part('SVGs/inline', 'logoSM.svg'); ?></a>
			<ul class="navigation-list">
				<?php wp_nav_menu( array( 'menu' => 'main', 'container' => '', 'items_wrap' => '%3$s', 'depth' => '1', 'orderby' => 'menu_order') ); ?>
				<a id="desktop-logo" class="nav-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php get_template_part('SVGs/inline', 'logoSM.svg'); ?></a>
			</ul>
			<div id="hamburger-button">
				<div class="hamburger-line"></div>
				<div class="hamburger-line"></div>
				<div class="hamburger-line"></div>
				<!-- <div class="hamburger-line"></div> -->
			</div>
		</nav> 
	</header><!-- #masthead -->

	<div id="content" class="site-content">
