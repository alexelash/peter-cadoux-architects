<?php
/**
 * Jetpack Compatibility File
 * See: https://jetpack.me/
 *
 * @package Peter Cadoux Architects
 */

/**
 * Add theme support for Infinite Scroll.
 * See: https://jetpack.me/support/infinite-scroll/
 */
function peter_cadoux_architects_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'render'    => 'peter_cadoux_architects_infinite_scroll_render',
		'footer'    => 'page',
	) );
} // end function peter_cadoux_architects_jetpack_setup
add_action( 'after_setup_theme', 'peter_cadoux_architects_jetpack_setup' );

/**
 * Custom render function for Infinite Scroll.
 */
function peter_cadoux_architects_infinite_scroll_render() {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'template-parts/content', get_post_format() );
	}
} // end function peter_cadoux_architects_infinite_scroll_render
