<?php
/**
 * The template used for displaying PRESS content in page_index.php
 *
 * @package Peter Cadoux Architects
 */

?>
<?php 
	$pageID = 10;
	$page = get_post($pageID);
	$curID = $page->ID;
	$pageTitle = get_the_title( $pageID );
	$bodyText = apply_filters( 'the_content',  get_post_field( 'post_content', $pageID ) );

	echo "<div class='content-container'>";
		echo "<h2 class='section-title'>$pageTitle</h2>";
		echo "<div class='section-introduction'>$bodyText</div>";
	echo "</div>";

	$curYear = date('Y');


	$postType = 'press';
	$args = array(
		'post_type' 			=> $postType,
		'post_status'			=> 'publish',
		'order'						=> 'DESC',
		'orderby'          => 'date',
		'posts_per_page'	=> -1
	);

	$pressItems = new WP_Query($args);
	echo "<ul class='press-list'>";
		if ($pressItems->have_posts()) {
			while ($pressItems->have_posts()) : $pressItems->the_post();
				$pressSlug = $post->post_name;
				
				if (is_old_post()) {
					// do nothing, this is a post that is over ten years old! Within functions.php
				} else {
					echo "<li id='$pressSlug' class='press-list-item'>";
						$pressTitle = get_the_title();
						$pressItemID = $post->ID;
						$pressPub = get_field('magazine');
						$dDate = get_field('display_date');
						$postDate = get_the_time('F j, Y', $pressItemID);
						$pressDate = ($dDate!=NULL)?$dDate:$postDate;

						echo "<h3 class='press-list-item-title'>$pressPub</h3>";
						echo "<h4 class='press-list-item-info'>$pressTitle, $pressDate</h4>";
					echo "</li>";
				}
			endwhile;
		}
	echo "</ul>";
	wp_reset_query();  // Restore global post data stomped by the_post().
 ?>
