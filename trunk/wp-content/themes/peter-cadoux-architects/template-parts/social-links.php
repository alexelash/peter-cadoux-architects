<footer class="contact-social">
	<div class="social-link">
		<a class="social-link-icon" href="https://pinterest.com/" target="_blank">
			<?php echo file_get_contents(get_template_directory_uri().'/layout-images/social_icons_pinterest.svg'); ?>
		</a>
	</div>
	<div class="social-link">
		<a class="social-link-icon" href="https://www.houzz.com/" target="_blank">
			<?php echo file_get_contents(get_template_directory_uri().'/layout-images/social_icons_houzz.svg'); ?>
		</a>
	</div>
</footer><!-- .entry-footer -->