<?php
/**
 * The template used for displaying HOMEPAGE content in page_index.php
 *
 * @package Peter Cadoux Architects
 */
?>
<div id="homepage-slideshow" class="slideshow">
	<?php 
		$images = get_field('homepage_images', $curID);
		$randomStart = rand(0, count($images));
		if( $images ){
			for ($i=0; $i < count($images); $i++) { 
				$modI = $i%count($images);
				$width = $images[$modI]['width'];
				$height = $images[$modI]['height'];
				$url = $images[$modI]['url'];
				$images[$modI]['description']?$image_location = $images[$modI]['description']:$image_location = 'center';
				
				echo "<div id='homeimg-$i' class='slide'><div class='image-holder' style='background-image:url($url);background-position:center " . $image_location . ";' data-width='$width' data-height='$height'><img class='loader-image' style='display:none;' src='$url'/></div></div>";
			}
		}
		echo "</div>";
		echo "<header>";
			echo "<div class='logo-holder'>";?>
				<h1 class='site-title'><?php get_template_part('SVGs/inline', 'logo.svg'); ?></h1>
				<span class='instruction show'><?php get_template_part('SVGs/inline', 'arrow.svg'); ?></span>
			<?php
			echo "</div>";
			echo "<div class='section-text'>";
				echo apply_filters('the_content', get_post_field('post_content', $curID));
			echo "</div>";
		echo "</header>";
	?>