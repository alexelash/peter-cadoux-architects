<?php
/**
 * The template used for displaying CONTACT PAGE content in page_index.php
 *
 * @package Peter Cadoux Architects
 */
?>
<div id="contact">
	<?php 
		// echo "<h2 class='section-title'>Contact</h2>";
		$images = get_field('images', $curID);
		if( $images ){
			foreach ($images as $i) {
				$width = $i['width'];
				$height = $i['height'];
				$url = $i['url'];
				echo "<div class='contact-image background-image image-holder' style='background-image:url($url);background-position:center;background-size:cover;' data-width='$width' data-height='$height'><img class='loader-image' style='display:none;' src='$url'/></div>";
			}
		}
		echo "</div>";
		echo "<footer class='site-footer'>";
			echo "<div class='logo-holder'>";?>
				<h1 class='site-title desktop-only-title'><?php get_template_part('SVGs/inline', 'logo.svg'); ?></h1>
				<p class="mobile-only-title">PETER CADOUX ARCHITECTS</p>
			<?php
			echo "</div>";
			echo "<div class='section-text'>";
				echo apply_filters('the_content', get_post_field('post_content', $curID));

				//include( 'social-links.php' );
			echo "</div>";
			dyad_credit();
		echo "</footer>";
	?>