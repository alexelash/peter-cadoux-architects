<?php
/**
 * The template used for displaying PROFILE content in page_index.php
 *
 * @package Peter Cadoux Architects
 */
?>

<?php 
	$pageID = 126;
	$page = get_post($pageID);
	$curID = $page->ID;
	$pageTitle = get_the_title( $pageID );
	$bodyText = apply_filters( 'the_content',  get_post_field( 'post_content', $pageID ) );
	$headerImage = get_field( 'thumbnail_image', $pageID );
	$headerImageURL =	$headerImage[url];
	$resizedURL = aq_resize($headerImageURL, 500);
	// prettyPrint($headerImage);

	echo "<div class='content-container'>";
		echo "<h2 class='section-title'>$pageTitle</h2>";
		echo "<div class='section-image textpage-image image-holder'>";
			echo "<img class='loader-image' src='$headerImageURL'>";
		echo "</div>";
		echo "<div class='section-text'>$bodyText</div>";
	echo "</div>";

	$args = array(
		'post_type' 			=> 'page',
		'post_parent'			=> $pageID,
		'order'						=> 'ASC',
		'orderby' 				=> 'menu_order',
		'showposts'				=> -1
	);
	$subpages = get_posts($args);
	$subpagesNum = 0;

		foreach( $subpages as $subpage ) {
			$subpagesNum++;
			$subpageID = $subpage->ID;
			$subpageTitle = get_the_title( $subpageID );
			$subpageSubtitle = get_field( 'position', $subpageID );
			$subpageText = apply_filters( 'the_content', get_post_field( 'post_content', $subpageID ) );
			$subheaderImageURL = NULL;
			$subheaderImage = get_field( 'thumbnail_image', $subpageID );
			if (is_array($subheaderImage) && $subheaderImage!='') {
				$subheaderImageURL =	$subheaderImage['url'];
			}
			$resizedURL = aq_resize($subheaderImageURL, 500);

		echo "<div id='sub-section-$subpagesNum' class='sub-section'>";
			echo "<h2 class='section-title'>$subpageTitle</h2>";	
			echo "<h3 class='section-subtitle'>$subpageSubtitle</h3>";	
			if ($subheaderImageURL) {
				echo "<div class='section-image textpage-image image-holder'>";
					echo "<img class='loader-image' src='$resizedURL'>";
				echo "</div>";
			}
			echo "<div class='section-text'>";
				echo "<div class='section-body'>$subpageText</div>";
			echo "</div>";
		echo "</div>";
		} // End Foreach loop
 ?>