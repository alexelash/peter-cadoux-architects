<?php
/**
 * 
 * Template Name: Ajax Project
 * Description: The template used for displaying PROJECT PAGE content in page_index.php
 *
 * @package Peter Cadoux Architects
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php 
					global $post;
					$projectTitle = get_the_title();
					$projectSlug = $post->post_name;
					$projectID = $post->ID;
					$siteURL = site_url();

					$images = get_field('slideshow_images', $projectID);
					echo "<div class='project-text'>";
						echo "<div class='share-button'>Share</div>";
						echo "<h3 class='project-title'>$projectTitle</h3>";
					 	echo "<span class='left-arrow arrows'>";
					 		get_template_part('SVGs/inline', 'arrow.svg');
					 	echo "</span>";
					 	echo "<span class='num-index'><span id='numer'>1</span> / <span id='denom'>0</span></span>";
					 	echo "<span class='right-arrow arrows'>";
					 		get_template_part('SVGs/inline', 'arrow.svg');
					 	echo "</span>";
					echo "</div>";
				 	echo "<div id='$projectSlug-slideshow' rel='$projectSlug' class='project-slideshow slideshow'>";
				// 	$randomStart = rand(0, count($images));
					if( $images ){
						$imgCount = 0;
						foreach ($images as $i) {
							$width = $i['width'];
							$height = $i['height'];
							$url = $i['url'];
							$resizedUrl = aq_resize( $url, 1501);
							if (is_mobile()) $resizedUrl = aq_resize( $url, 800);
           		elseif ($resizedUrl=="") $resizedUrl = $iURL;
							$proportions = ($width > $height)?'wide':'tall';
							$srcString = "data-src='$resizedUrl'";
							$loadClass = "delayed-load";
							if($imgCount<2 || is_mobile()) {
								$srcString = "src='$resizedUrl'";
								$loadClass = "";
							}

							$ce_image->make($url,
								array(
									'fallback_src' => $url
								)
							);
							$markedurl = $ce_image->get_relative_path();
							// pp($markedurl);
							// $debug = $ce_image->get_debug_messages();
							// prettyPrint( $debug, $markedurl );
							$ce_image->close();

							
							echo "<div class='slide'>";
								echo "<div class='$loadClass image-holder $proportions' style='background-image:url($url);background-position:center;' data-width='$width' data-height='$height'>";
									echo "<img class='loader-image' style='display:none;' $srcString />";
									//echo "<div class='ring' style='background-image:url($siteURL/ring.gif);'></div>";
								echo "</div>";
								share_link($markedurl, get_the_permalink(), $projectTitle);
							echo "</div>";
							$imgCount++;
						}
					}
				 	echo "</div>";
					// }
				?>
			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
