<?php
/**
 * Peter Cadoux Architects functions and definitions
 *
 * @package Peter Cadoux Architects
 */

function pp($a,$b = '') {
	echo '<pre>'.print_r($a,1).'<br/>'.$b.'</pre>';
}
add_filter( 'wp_title', 'wpdocs_hack_wp_title_for_home' );
 
/**
 * Customize the title for the home page, if one is not set.
 *
 * @param string $title The original title.
 * @return string The title to use.
 */
function wpdocs_hack_wp_title_for_home( $title ) {
  // if ( empty( $title )) {
    $title = __( 'Peter Cadoux Architects', 'textdomain' ) . ' | ' . 'Homepage' ;
  // }
  return $title;
}
// If post is older than 10 years
function is_old_post($days = 3600) {
	$days = (int) $days;
	$offset = $days*60*60*24;
	if ( get_post_time() < date('U') - $offset ) return true;
	
	return false;
}
function siblings($link) {
    global $post;
    $siblings = get_pages('child_of='.$post->post_parent.'&parent='.$post->post_parent.'&sort_column=menu_order');
    foreach ($siblings as $key=>$sibling){
        if ($post->ID == $sibling->ID){
            $ID = $key;
        }
    }
    $closest = array('before'=>$siblings[$ID-1],'after'=>$siblings[$ID+1]);

    return $closest;
}
require_once('aq_resizer.php');
require_once 'Ce_image.php';

$ce_image = new Ce_image( array(
	'cache_dir' => '/pca/trunk/cache/',
	'memory_limit' => 3000,
	'watermark' => array(
		$watermark_src = '/Library/WebServer/NewDev/pca/trunk/wp-content/themes/peter-cadoux-architects/layout-images/watermark.gif',
		$minimum_dimensions = array( 0, 0 ),
		$opacity = 75,
		$position = array( 'right', 'bottom' ),
		$offset = array( -20, -20 ),
	)
));

function share_link($markedurl, $projecturl, $projecttitle){
	$twitterHandle = "";
	$company_name = "Peter Cadoux Architects";
	$websiteEncodedURL = htmlentities(get_site_url());
	$websiteRoot = 'http://dev.dyadcom.com/';
	$hzid = '34098';

	$pinterestSVG = '<svg class="share-icon" viewBox="0 0 33 33" width="25" height="25"><g><path d="M 16.5,0C 7.387,0,0,7.387,0,16.5s 7.387,16.5, 16.5,16.5c 9.113,0, 16.5-7.387, 16.5-16.5S 25.613,0, 16.5,0z M 18.1,22.047 c-1.499-0.116-2.128-0.859-3.303-1.573C 14.15,23.863, 13.36,27.113, 11.021,28.81 c-0.722-5.123, 1.060-8.971, 1.888-13.055c-1.411-2.375, 0.17-7.155, 3.146-5.977 c 3.662,1.449-3.171,8.83, 1.416,9.752c 4.79,0.963, 6.745-8.31, 3.775-11.325 c-4.291-4.354-12.491-0.099-11.483,6.135c 0.245,1.524, 1.82,1.986, 0.629,4.090c-2.746-0.609-3.566-2.775-3.46-5.663 c 0.17-4.727, 4.247-8.036, 8.337-8.494c 5.172-0.579, 10.026,1.898, 10.696,6.764 C 26.719,16.527, 23.63,22.474, 18.1,22.047z"></path></g></svg>';
	$houzzSVG = '<svg class="share-icon" width="25" height="25" viewBox="189.498 753.751 19.835 34.645"><g><polygon fill="#231F20" points="199.4,788.4 209.3,782.5 209.3,771.1 199.4,777"/><polygon fill="#231F20" points="199.4,765.5 209.3,771.1 209.3,759.6"/><g><polygon fill="#231F20" points="199.9,766.8 199.9,775.7 207.5,771.2"/></g><g><polygon fill="#231F20" points="199,775.7 199,766.8 191.5,771.3"/></g></g><g><polygon fill="#231F20" points="189.5,771.4 189.5,782.9 199.4,777"/><polygon fill="#231F20" points="199.4,753.8 189.5,759.6 189.5,771.4 199.4,765.5"/></g></svg>';
	$twitterSVG = '<svg class="share-icon" viewBox="0 0 33 33" width="25" height="25"><g><path d="M 32,6.076c-1.177,0.522-2.443,0.875-3.771,1.034c 1.355-0.813, 2.396-2.099, 2.887-3.632 c-1.269,0.752-2.674,1.299-4.169,1.593c-1.198-1.276-2.904-2.073-4.792-2.073c-3.626,0-6.565,2.939-6.565,6.565 c0,0.515, 0.058,1.016, 0.17,1.496c-5.456-0.274-10.294-2.888-13.532-6.86c-0.565,0.97-0.889,2.097-0.889,3.301 c0,2.278, 1.159,4.287, 2.921,5.465c-1.076-0.034-2.088-0.329-2.974-0.821c-0.001,0.027-0.001,0.055-0.001,0.083 c0,3.181, 2.263,5.834, 5.266,6.438c-0.551,0.15-1.131,0.23-1.73,0.23c-0.423,0-0.834-0.041-1.235-0.118 c 0.836,2.608, 3.26,4.506, 6.133,4.559c-2.247,1.761-5.078,2.81-8.154,2.81c-0.53,0-1.052-0.031-1.566-0.092 c 2.905,1.863, 6.356,2.95, 10.064,2.95c 12.076,0, 18.679-10.004, 18.679-18.68c0-0.285-0.006-0.568-0.019-0.849 C 30.007,8.548, 31.12,7.392, 32,6.076z"></path></g></svg>';
	$fbSVG = '<svg class="share-icon" viewBox="0 0 33 33" width="25" height="25"><g><path d="M 17.996,32L 12,32 L 12,16 l-4,0 l0-5.514 l 4-0.002l-0.006-3.248C 11.993,2.737, 13.213,0, 18.512,0l 4.412,0 l0,5.515 l-2.757,0 c-2.063,0-2.163,0.77-2.163,2.209l-0.008,2.76l 4.959,0 l-0.585,5.514L 18,16L 17.996,32z"></path></g></svg>';
	$emailSVG = '<svg class="share-icon" x="0px" y="0px" width="25" height="25" viewBox="0 0 122.678 96.391" enable-background="new 0 0 122.678 96.391" xml:space="preserve"><path d="M122.678,10.952v74.487c0,3.005-1.068,5.581-3.215,7.727c-2.146,2.146-4.722,3.225-7.737,3.225H10.952
		c-3.015,0-5.591-1.078-7.737-3.225C1.068,91.02,0,88.444,0,85.439V10.952c0-3.015,1.068-5.591,3.215-7.737S7.937,0,10.952,0
		h100.774c3.015,0,5.591,1.068,7.737,3.215S122.678,7.937,122.678,10.952z M113.912,13.488c0-0.09,0-0.349,0-0.759s0-0.719,0-0.918
		c0-0.21-0.01-0.509-0.03-0.889c-0.03-0.389-0.09-0.679-0.21-0.859c-0.11-0.18-0.24-0.389-0.369-0.619
		c-0.14-0.23-0.349-0.399-0.619-0.509c-0.28-0.12-0.599-0.17-0.958-0.17H10.952c-0.589,0-1.108,0.21-1.537,0.649
		c-0.439,0.429-0.649,0.948-0.649,1.537c0,7.667,3.354,14.147,10.063,19.438c8.805,6.939,17.96,14.177,27.445,21.704
		c0.28,0.23,1.078,0.899,2.396,2.017c1.328,1.128,2.376,1.977,3.155,2.576c0.779,0.589,1.787,1.308,3.045,2.156
		c1.258,0.839,2.406,1.468,3.454,1.877c1.048,0.409,2.037,0.619,2.945,0.619h0.07h0.07c0.909,0,1.887-0.21,2.945-0.619
		c1.048-0.409,2.196-1.038,3.454-1.877c1.258-0.849,2.266-1.567,3.045-2.156c0.779-0.599,1.827-1.448,3.155-2.576
		c1.318-1.118,2.117-1.787,2.396-2.017c9.484-7.528,18.639-14.766,27.445-21.704c2.466-1.957,4.762-4.592,6.879-7.907
		C112.854,19.178,113.912,16.183,113.912,13.488z M113.912,85.439V32.856c-1.458,1.647-3.035,3.155-4.722,4.523
		C96.96,46.783,87.236,54.49,80.028,60.52c-2.326,1.957-4.223,3.484-5.681,4.582c-1.468,1.098-3.434,2.206-5.92,3.325
		c-2.496,1.118-4.832,1.677-7.018,1.677h-0.07h-0.07c-2.186,0-4.533-0.559-7.018-1.677s-4.463-2.226-5.92-3.325
		c-1.458-1.098-3.354-2.626-5.681-4.582c-7.208-6.03-16.932-13.737-29.162-23.142c-1.687-1.368-3.265-2.875-4.722-4.523v52.583
		c0,0.589,0.21,1.098,0.649,1.537c0.429,0.429,0.948,0.649,1.537,0.649h100.774c0.589,0,1.108-0.22,1.537-0.649
		C113.702,86.537,113.912,86.028,113.912,85.439z"/>
		</svg>
		';
	?>
	<div class="share-link">
		<!-- <div class="mobile-share-button">Share</div> -->
		<div class="social-links">
			<a target="_blank" href="https://www.pinterest.com/pin/create/button/?url=<?php echo $projecturl; ?>&media=<?php echo $websiteRoot.$markedurl; ?>&description=From the Portfolio of <?php echo $company_name; ?>" data-pin-do="buttonPin" data-pin-config="none" data-pin-color="white">
				<?php echo $pinterestSVG ?>
				Pin
			</a>
			<a href="https://www.facebook.com/sharer/sharer.php?app_id=309437425817038&sdk=joey&u=<?php echo $projecturl; ?>&display=popup&ref=plugin&src=share_button" target="_blank">
				<?php echo $fbSVG ?>
				Share
			</a>
			<a class="button houzz-share-button" 
				 data-hzid=<?php echo $hzid ?>
				 data-title="<?php echo $company_name ?>" 
				 data-img="<?php echo $websiteEncodedURL; ?><?php echo $markedurl; ?>"
				 data-locale="en-US" 
				 data-showcount="1"
				 target="_blank" 
				 href='http://www.houzz.com/imageClipperUpload?link=<?php echo urlencode($projecturl); ?>&source=button&hzid=<?php echo $hzid ?>&imageUrl=<?php echo $websiteRoot.$markedurl; ?>&title=<?php echo urlencode($company_name)?>&ref=<?php echo urlencode($projecturl); ?>'>
					<?php echo $houzzSVG ?>
					Houzz
			</a>
			<!-- <a class='button tweet-button' target="_blank" href='https://twitter.com/intent/tweet?text=See+the+<?php echo urlencode($projecttitle)?>+project+by+<?php echo urlencode($company_name)?>+at+&via=<?php echo $twitterHandle?>&url=<?php echo urlencode($projecturl); ?>'>
				<?php echo $twitterSVG ?>
				Tweet</a> -->
			<a class="button email-link" href="mailto:?subject=<?php echo $projecttitle?> by <?php echo $company_name ?>&body=<?php echo $websiteRoot.$markedurl; ?>">
			<?php echo $emailSVG ?> 
			Email</a>
		</div>
	</div>
	<?php 
}
function dyad_credit(){
?>
	<style type="text/css" media="screen">
		@font-face {
			font-family: "MrsEaves";
			src: url('<?php echo get_template_directory_uri();?>/webfonts/mrseaves/MrsEaves-rom.woff') format('woff');
			src: url('<?php echo get_template_directory_uri();?>/webfonts/mrseaves/MrsEaves-rom.eot');
			/*src: url('<?php echo get_template_directory_uri();?>/webfonts/mrseaves/MrsEaves-rom.eot') format('eot');*/
			font-weight: normal;
			font-style: normal;
		}

		@font-face {
			font-family: "MrsEaves";
			src: url('<?php echo get_template_directory_uri();?>/webfonts/mrseaves/MrsEaves-ita.woff') format('woff');
			src: url('<?php echo get_template_directory_uri();?>/webfonts/mrseaves/MrsEaves-ita.eot');
			/*src: url('<?php echo get_template_directory_uri();?>/webfonts/mrseaves/MrsEaves-rom.eot') format('eot');*/
			font-weight: normal;
			font-style: italic;
		}
		#credit{
			display:none;
		}
		#credit{
			margin: 0;	
			padding: 0;		
			text-align: center;
			
			line-height: 2;
			font-family: MrsEaves;
			font-style: normal;
			overflow: hidden;
			font-size: 0.9rem;
		  visibility: hidden;
			text-align: center;
			display: inline-block;
			opacity: 0.75;
			z-index: 9999;
			color: #ADADAD!important;
	    position: absolute;
	    bottom: 1.65rem;
	    right: 2rem;
	    /*left: 1rem;*/
			
			width: auto;
			text-decoration: none;
	    -webkit-font-smoothing: antialiased;
	  	-moz-osx-font-smoothing: grayscale;
		}
		#credit #dyad{
			text-transform: uppercase;
			/*color: #bcbcb7;*/
		}
		#credit span#holder{
			white-space:nowrap;
		}
		/*	#therest, #siteby {
				display: none;
			}*/
			/*.touch #therest, .touch #siteby {
				display: inline-block;
				
			}*/
		}
</style>
<script type="text/javascript" charset="utf-8">
	window.onload = function() {
		initDyadCredit();
		function initDyadCredit(){
      setTimeout("$('#credit').css('visibility','visible');",2200);
			// if(!('ontouchstart' in document.documentElement)) 
			hideCredit( $('#credit'), 0);
		

			$('#credit').hover(function(){
				revealCredit( $(this), .5);
			},function(){
				hideCredit( $(this), .5);
			});
	
			function hideCredit(c,dur){
				// if('ontouchstart' in document.documentElement) return; 
				TweenLite.to(c, dur, {width:c.find('#d').width()});
				TweenLite.to(c.find('#therest'), dur, {opacity:0});
				TweenLite.to(c.find('span#holder'), dur, {marginLeft:-1*c.find('span#siteby').width()});
			}
	
			function revealCredit(c,dur){
				if('ontouchstart' in document.documentElement) return; 
				TweenLite.to(c, dur, {width:c.find('span#holder').width()});
				TweenLite.to(c.find('#therest'), dur, {opacity:1});
				TweenLite.to(c.find('span#holder'), dur, {marginLeft:0});
			}
		}
	};
</script>
<a id="credit" href="http://www.dyadcom.com" target="_blank"><span id="holder"><span id="siteby"><em>Site by</em> </span><span id="dyad"><span id="d">D</span><span id="therest">yad Communications</span></span></span></a>
<?php
}




if ( ! function_exists( 'peter_cadoux_architects_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function peter_cadoux_architects_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Peter Cadoux Architects, use a find and replace
	 * to change 'peter-cadoux-architects' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'peter-cadoux-architects', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'peter-cadoux-architects' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'peter_cadoux_architects_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // peter_cadoux_architects_setup
add_action( 'after_setup_theme', 'peter_cadoux_architects_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function peter_cadoux_architects_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'peter_cadoux_architects_content_width', 640 );
}
add_action( 'after_setup_theme', 'peter_cadoux_architects_content_width', 0 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function peter_cadoux_architects_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'peter-cadoux-architects' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'peter_cadoux_architects_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function peter_cadoux_architects_scripts() {
	wp_enqueue_style ( 'peter-cadoux-architects-style', get_stylesheet_uri() );
	wp_enqueue_script( 'peter-cadoux-architects-plugins', get_template_directory_uri() . '/js/plugins.js');
	wp_enqueue_script( 'peter-cadoux-architects-main', get_template_directory_uri() . '/js/main.js');
	// wp_enqueue_script( 'ajax-projects',  get_stylesheet_directory_uri() . '/js/ajax-projects.js' );

	// wp_localize_script( 'ajax-projects', 'ajaxprojects', array(
	// 	'ajaxurl' => admin_url( 'admin-ajax.php' )
	// ));

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'peter_cadoux_architects_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
