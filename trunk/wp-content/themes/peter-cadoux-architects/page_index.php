<?php
/**
 * Template Name: Main Template
 * Description: The template that displays the homepage images and tagline.
 *
 *
 *
 * @package Peter Cadoux Architects
 */

get_header(); ?>
<main class="site-main" role="main">
	<div class="animation-text">Creating Exceptional Residences</div>
		<?php
			$args = array(
				'order'				=> 'ASC',
				'sort_column' => 'menu_order',
				'parent' 			=> '0',
			);
			$pages = get_pages($args);
			foreach ($pages as $page) {
				// $pages_to_exclude = array(11);
				$slug = $page->post_name;
				$parent = get_post($page->post_parent);
				$parentName = strtolower($parent->post_title);
				$projectPages = $page->post_parent==8;
				$parentSlug = $projectPages?"projects closed":'';
				$curID = $page->ID;
				// prettyPrint($pages);

				echo "<section id='$slug' data-post-name='$slug' class='$parentSlug' data-sitename='Peter Cadoux Architects | $page->post_title'>";


					if ($slug=='homepage') {
						include(locate_template('template-parts/content-homepage.php'));
					} else if ($curID == 126){
						include(locate_template('template-parts/content-profile.php'));
					} else if ($curID == 10){
						include(locate_template('template-parts/content-press.php'));
					} else if ($curID == 12){
						include(locate_template('template-parts/content-contact.php'));
					} else if ($curID == 8) {
						$args = array(
							'post_type' 			=> 'page',
							'post_parent'			=> 8,
							'order'						=> 'ASC',
							'orderby' 				=> 'menu_order',
							'showposts'				=> -1
						);
						$projects = get_posts($args);
						$projCount = count($projects);

						echo "<div id='project-holder' class='hidden-project sub-section'></div>";

						echo "<div id='projects-con' data-post-name='projects' data-sitename='Peter Cadoux Architects | Projects' class='gallery-columns-4'>";
							echo "<h2 class='section-title'>Projects</h2>";
								foreach( $projects as $project ) {
									$title = $project->post_title;
									$slug = $project->post_name;
									$id = $project->ID;
									$permalink = get_page_link($id);
									$thumbnail = get_field('thumbnail_image', $project->ID);
									$thumbnailURL = aq_resize($thumbnail['url'], 800, 550, true);
									$small_thumbnailURL = aq_resize($thumbnail['url'], 200);

									echo "<a href='$permalink' data-post-name='$slug' id='$slug-thumb' rel='$id' class='project thumb'>";
										echo "<div class='section-image project-image image-holder'>";
											echo "<img class='loader-image' src='$thumbnailURL'>";
										echo "</div>";
										echo "<h3 class='project-title'>$title</h3>";
										// echo "<div class='expand-btn btn'>View Project</div>";
									echo "</a>";
								}
						echo "</div>";
					}
				echo "</section>";

			} // Page loop ?>



</main>

<?php get_footer(); ?>
